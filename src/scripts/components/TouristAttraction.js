export class TouristAttraction {
    constructor() {
        this.list = [];

        this.selectors();
        this.events();
    }

    selectors() {
        this.form = document.querySelector(".form");
        this.cards = document.querySelector(".cards");
        this.inputImage = document.querySelector(".form__image__input");
        this.label = document.querySelector(".form__image__label");
    }

    events() {
        this.inputImage.addEventListener("change", this.previewImage.bind(this));
        this.form.addEventListener("submit", this.addTouristAttraction.bind(this));
    }

    previewImage(event){
        let image = window.URL.createObjectURL(event.target.files[0]);
        this.label.innerHTML = '';

        this.label.style.backgroundImage = `url("${image}")`;
    }

    addTouristAttraction(event) {
        event.preventDefault();

        const dataForm = new FormData(this.form);
        let image = undefined;

        if (dataForm.get("imageForm").name === "") {
            image = "./assets/images/image-notFound.jpg";
        } else {
            image = window.URL.createObjectURL(dataForm.get("imageForm"));
        }

        const card = {
            title: dataForm.get("titleForm"),
            description: dataForm.get("descriptionForm"),
            image: image,
        };

        this.list.push(card);
        this.render();
        this.resetInputs(event);
    }

    render() {
        let cardStructure = ``;

        this.list.forEach((e) => {
            cardStructure += `
            <div class="card">
                <img
                    class="card__image"
                    src="${e.image}"
                    alt="Imagem ${e.title}"
                />
                <div class="card__text">
                    <h2 class="card__title">${e.title}</h2>
                    <p class="card__description">
                        ${e.description}
                    </p>
                </div>
            </div>
        `;
        });

        this.cards.innerHTML = cardStructure;
    }

    resetInputs(eve) {
        eve.target["titleForm"].value = "";
        eve.target["descriptionForm"].value = "";
        eve.target["imageForm"].value = "";
        this.label.innerHTML = 'Imagem';
        this.label.style.backgroundImage = 'none';
    }
}
